# Delta-DVPSX2-Programmable-Logic-Controllers

The DVP-SX2 Series is a thin type PLC with 14 digital points and built-in 12-bit analogue I/O. It is the second version of the DVP-SX2 Series. It may be expanded with left- and right-side expansion modules from the DVP-S series. 